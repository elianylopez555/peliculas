
const cardMovies = document.querySelector('#cardMovies');
const tempCard = document.querySelector('#tempCard').content;
const fragment = document.createDocumentFragment();

document.addEventListener("DOMContentLoaded", () => {
    obtenerinfo();
});

const obtenerinfo = async () => {
    try {
        cargandoInfo(true);

        const res = await fetch("https://api.themoviedb.org/3/movie/popular?api_key=77c8d8b6d46a79d2c5e7630e97a10274&language=es-MX");
        const data = await res.json();

        mostrarDatos(data);
    } catch (error) {
        console.log(error);
    } finally {
        cargandoInfo(false);
    }
};

const cargandoInfo = (estado) => {
    const cargando = document.getElementById("cargando");
    if (estado) {
        cargando.classList.remove("d-none");
    } else {
        cargando.classList.add("d-none");
    }
};

const mostrarDatos = (data) => {
  

    cardMovies.textContent = "";

    data.results.forEach((item) => {
        const clone = tempCard.cloneNode(true);
        clone.querySelector("h5").textContent = item.title;
        //clone.querySelector("p").textContent = item.species;
        //clone.querySelector("img").setAttribute("src", item.image);

        fragment.appendChild(clone);
    });
     cardMovies.appendChild(fragment);
};















/*

const tarjetas= document.getElementById("card-dinamicas");
const temple= document.getElementById("templateC").content;
const fragment= document.createDocumentFragment();

document.addEventListener("DOMContentLoaded", () =>{
    obtenerinfo()
})
const obtenerinfo= async () =>{
    
    try{
        cargandoDatos(true)
        const res = await fetch("https://rickandmortyapi.com/api/character");
        const data = await res.json();
        //console.log(data)
        mostrarData(data)
    }catch(err){

    }finally{
        cargandoDatos(false)
    }
}
//---para que me quite el cargando o me lo coloque
const cargandoDatos= estado =>{
    const cargando=document.getElementById("cargando");
    if (estado) {
        cargando.classList.remove("d-none");
    }else{
        cargando.classList.add("d-none");
    }
};
const mostrarDatos = (data) => {
   

    tarjetas.textContent = "";

    data.results.forEach((item) => {
        const clone = templateC.cloneNode(true);
        clone.querySelector("h5").textContent = item.name;
        clone.querySelector("p").textContent = item.species;
        clone.querySelector("img").setAttribute("src", item.image);

        fragment.appendChild(clone);
    });
    cards.appendChild(fragment);
};
*/