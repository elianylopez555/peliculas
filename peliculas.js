
const cardMovies = document.querySelector('#cardMovies');
const tempCard = document.querySelector('#tempCard').content;
const lista = document.querySelector('#lista');
const tempList = document.querySelector('#tempList').content;
const fragment = document.createDocumentFragment();
const listSelect = []; //peliculas seleccionadas



document.addEventListener("DOMContentLoaded", () => {
    obtenerinfo();
});


document.addEventListener("click", (e)=>{
	if (e.target.matches(".btn-outline-warning")) {
		next()
	}

	if (e.target.matches(".btn-outline-danger")) {
		previus()
	}
	if (e.target.matches(".btn-outline-light")) {
		agregar()
	}
})

let pagina = 1;
 function next(e) {
	if (pagina <1000) {
		pagina++;
		obtenerinfo()
	}
}

function previus(e) {
	
	if(pagina > 1){
		pagina-- ;
		obtenerinfo()
	}
}

const obtenerinfo = async () => {
    try {
        cargandoInfo(true);

        const res = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=192e0b9821564f26f52949758ea3c473&language=es-MX&page=${pagina}`);
        const data = await res.json();

        mostrarDatos(data);
        console.log(data);
    } catch (error) {
       // console.log(error);
    } finally {
        cargandoInfo(false);
    }
};

const cargandoInfo = (estado) => {
    const cargando = document.getElementById("cargando");
    if (estado) {
        cargando.classList.remove("d-none");
    } else {
        cargando.classList.add("d-none");
    }
};

const mostrarDatos = (data) => {
  

    cardMovies.textContent = "";

    data.results.forEach((item) => {
        const clone = tempCard.cloneNode(true); 
        clone.querySelector("h5").textContent = item.title;
       
        clone.querySelector("img").setAttribute("src","https://image.tmdb.org/t/p/w500"+ item.poster_path);

        fragment.appendChild(clone);
    });
     cardMovies.appendChild(fragment);
};

const agregar=(item)=>{
	console.log("bbb")
 	const movies={
		title: item.querySelector("h5").textContent,
		id: item.querySelector(".btn-outline-light").dataset.id,
		cantidad:1,
    };
    

      //buscamos el indice
   const indice = listSelect.findIndex((item) => item.id === movies.id);
  
    console.log(indice)
    // si no existe empujamos el nuevo elemento
    if (indice === -1) {
        listSelect.push(movies);
    } else {
     /// en caso contrario aumentamos su cantidad 
        listSelect[indice].cantidad++;}

    mostarList();
}

const mostrarList = (array) =>{
	lista.textContent = " ";

	listSelect.forEach(item =>{ 
		const clone = tempList.cloneNode(true);
		clone.querySelector(".title").textContent = item.title; 
		clone.querySelector(".rounded-pill").textContent = item.cantidad; 
		

		//capturando boton para q tome el id correspondiente
		clone.querySelector(".btn-outline-danger").dataset.id = item.id;
		clone.querySelector(".btn-outline-success").dataset.id = item.id;

		fragment.appendChild(clone);
	})
	lista.appendChild(fragment)
	total()       
} 














/*

const tarjetas= document.getElementById("card-dinamicas");
const temple= document.getElementById("templateC").content;
const fragment= document.createDocumentFragment();

document.addEventListener("DOMContentLoaded", () =>{
    obtenerinfo()
})
const obtenerinfo= async () =>{
    
    try{
        cargandoDatos(true)
        const res = await fetch("https://rickandmortyapi.com/api/character");
        const data = await res.json();
        //console.log(data)
        mostrarData(data)
    }catch(err){

    }finally{
        cargandoDatos(false)
    }
}
//---para que me quite el cargando o me lo coloque
const cargandoDatos= estado =>{
    const cargando=document.getElementById("cargando");
    if (estado) {
        cargando.classList.remove("d-none");
    }else{
        cargando.classList.add("d-none");
    }
};
const mostrarDatos = (data) => {
   

    tarjetas.textContent = "";

    data.results.forEach((item) => {
        const clone = templateC.cloneNode(true);
        clone.querySelector("h5").textContent = item.name;
        clone.querySelector("p").textContent = item.species;
        clone.querySelector("img").setAttribute("src", item.image);

        fragment.appendChild(clone);
    });
    cards.appendChild(fragment);
};
*/